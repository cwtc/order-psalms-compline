\include "../common_liturgy.ly"

\layout {
  ragged-last = ##t
}

% Markup to let lyrics wrap/line break
#(define-markup-list-command (wrap-lyrics layout props line) (markup-list?)
	#:properties ((line-width 40))
	"Lyric wrapping"
	(interpret-markup-list layout props
		(make-override-lines-markup-list (cons 'line-width line-width)
		(make-wordwrap-lines-markup-list line))))

OfficiantMusic = \relative c' {
	\clef bass
	\key e \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	g1 e4 e4 \bar "|" \noBreak
	g1 fis4 e4 g4 \bar "|"
	g1 c,4 c4 \bar "||"
	\cadenzaOff
}

OfficiantLyrics = \lyricmode {
	\override LyricText.self-alignment-X = #LEFT
	"Brethren, be sober, be vi" -- gi -- lant,
	"because your adversary, the"
	_ _ _
	"whom resist, steadfast in" the faith.
}

LyricsTwo = \lyricmode {
	\override LyricText.self-alignment-X = #LEFT
	_ _ _
	\markup { \hspace #4 "devil, as a roaring lion walketh" }
}

LyricsThree = \lyricmode {
	\override LyricText.self-alignment-X = #LEFT
	_ _ _
	\markup { \hspace #8 "about, seeking whom he" } may de -- vour;
}

\score {
	\new Staff <<
	\new Voice = "Officiant" {
		\OfficiantMusic
	}
	\new Lyrics \lyricsto "Officiant" {
		\OfficiantLyrics
	}
	\new Voice = "Two" {
		\OfficiantMusic
	}
	\new Lyrics \lyricsto "Two" {
		\LyricsTwo
	}
	\new Voice = "Three" {
		\OfficiantMusic
	}
	\new Lyrics \lyricsto "Three" {
		\LyricsThree
	}
	>>
}
