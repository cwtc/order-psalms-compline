\include "../common_liturgy.ly"

\layout {
  ragged-right = ##f
}

OfficiantMusic = \relative c' {
	\clef bass
	\key e \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	g1 e4 e4 \bar "|" \noBreak
	g1 fis4 e4 g4 \bar "|" \noBreak
	g1 c,4 c4 \bar "||" 
	\cadenzaOff
}

OfficiantLyrics = \lyricmode {
	\override LyricText.self-alignment-X = #LEFT
	"Thou, O Lord," %art in the midst" of us,
	%"and we are called" by thy name.
	%"Leave us not, O Lord" our God.
}

LyricsTwo = \lyricmode {
	\override LyricText.self-alignment-X = #LEFT
	%"Thou, O Lord, 
	\markup { \hspace #4 "art in the midst" } of us,
	"and we are called" by thy name.
	"Leave us not,"% O Lord" our God.
}

LyricsThree = \lyricmode {
	\override LyricText.self-alignment-X = #LEFT
	%"Thou, O Lord, 
	%\markup { \hspace #4 "art in the midst" } of us,
	%"and we are called" by thy name.
	%"Leave us not, 
	_ _ _
	_ _ _ _
	\markup { \hspace #4 "O Lord" } our God.
}

\score {
	\new Staff <<
	\new Voice = "Officiant" {
		\OfficiantMusic
	}
	\new Lyrics \lyricsto "Officiant" {
		\OfficiantLyrics
	}
	\new Voice = "V2" {
		\OfficiantMusic
	}
	\new Lyrics \lyricsto "V2" {
		\LyricsTwo
	}
	\new Voice = "V3" {
		\OfficiantMusic
	}
	\new Lyrics \lyricsto "V3" {
		\LyricsThree
	}
	>>
}