\include "../common_liturgy.ly"

\layout {
  ragged-right = ##f
}

OfficiantMusic = \relative c' {
	\clef bass
	\key e \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	g1 e4 \bar "|" \break
	g1 fis4 e4 g4 \bar "|" \noBreak
	g1 e4 e4 \bar "|"
	g1 fis4 e4 g4 \bar "|" \noBreak
	g1 c,4 \bar "||"
	\cadenzaOff
}

OfficiantLyrics = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	"Now the God of peace that brought again"
}

LyricsTwo = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	\markup {\hspace #16 "from the dead our Lord Jesus, that great shepherd of the"} sheep,
	"through the blood of the ever"-- last -- ing covenant,
	"make you perfect in"
}

LyricsThree = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	_ _
	_ _ _ _
	\markup {\hspace #2 "every good work to do" } his will,
	"working in you that"
}

LyricsFour = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	_ _
	_ _ _ _
	_ _ _
	\markup {\hspace #4 "which is well pleasing" } in his sight,
	"through Jesus Christ, to"
}

LyricsFive = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	_ _
	_ _ _ _
	_ _ _
	_ _ _ _
	\markup {\hspace #4 "whom be glory forever and ev" } -- er.
}

\score {
	\new Staff <<
	\new Voice = "Officiant" {
		\OfficiantMusic
	}
	\new Lyrics \lyricsto "Officiant" {
		\OfficiantLyrics
	}
	\new Voice = "V2" {
		\OfficiantMusic
	}
	\new Lyrics \lyricsto "V2" {
		\LyricsTwo
	}
	\new Voice = "V3" {
		\OfficiantMusic
	}
	\new Lyrics \lyricsto "V3" {
		\LyricsThree
	}
	\new Voice = "V4" {
		\OfficiantMusic
	}
	\new Lyrics \lyricsto "V4" {
		\LyricsFour
	}
	\new Voice = "V5" {
		\OfficiantMusic
	}
	\new Lyrics \lyricsto "V5" {
		\LyricsFive
	}
	>>
}
