\include "../common_liturgy.ly"

\layout {
  %ragged-right = ##t
}

% Markup to let lyrics wrap/line break
#(define-markup-list-command (wrap-lyrics layout props line) (markup-list?)
	#:properties ((line-width 40))
	"Lyric wrapping"
	(interpret-markup-list layout props
		(make-override-lines-markup-list (cons 'line-width line-width)
		(make-wordwrap-lines-markup-list line))))

OfficiantMusic = \relative c' {
	\clef bass
	\key e \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	g1 fis4 e4 g4 \bar "|"
	g1 c,4 c4 \bar "|"
	g'1 e4 e4 \bar "|" 
	g1 fis4 e4 g4 \bar "|"
	g1 c,4 c4 \bar "|"
	g'4 g4 fis4 e4 g2 g4 c,4 \bar "||"
	\cadenzaOff
}

OfficiantLyrics = \lyricmode {
	\override LyricText.self-alignment-X = #LEFT
	"Come unto me, "%all ye that labour and are" hea -- vy laden,
	%"and I will give" you rest.
	%"Take my yoke upon you, and learn" of me,
	%"for I am meek and lowly" in heart,
	%"and ye shall find rest unto" your souls.
	%For my yoke is "easy, and my burden" is light.
}

LyricsTwo = \lyricmode {
	\override LyricText.self-alignment-X = #LEFT
	\markup { \hspace #8 "all ye that labour and are" } hea -- vy laden, \noBreak
	"and I will give" you rest.
	"Take my yoke upon you, and learn" of me, \noBreak
	"for I am meek and low" -- ly in heart,
	"and ye shall find rest unto" your souls. \noBreak
	For my yoke is "easy, and my burden" is light.
}

\score {
	\new Staff <<
	\new Voice = "Officiant" {
		\OfficiantMusic
	}
	\new Lyrics \lyricsto "Officiant" {
		\OfficiantLyrics
	}
	\new Voice = "V2" {
		\OfficiantMusic
	}
	\new Lyrics \lyricsto "V2" {
		\LyricsTwo
	}
	>>
}