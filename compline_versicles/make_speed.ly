\include "../common_liturgy.ly"

\relative c' {
	\clef bass
	\key e \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	\ph
	e,4 g4 g4 a4 g4 fis4 g2 \bar "||" 
	\cadenzaOff
}
\addlyrics {
	\override LyricText.self-alignment-X = #LEFT
	%\markup { \versicle { O } } God, make speed to save us.
	\Vbar O God, make speed to save us.
}
\addlyrics {
	\override LyricText.self-alignment-X = #LEFT
	%\markup { \response { O } } Lord, make haste to help us.
	\Rbar O Lord, make haste to help us.
}