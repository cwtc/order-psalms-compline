\include "../common_liturgy.ly"

\relative c' {
	\clef bass
	\key e \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	g4 a4 g4( fis4) fis2 \bar "||" 
	\cadenzaOff
}
\addlyrics {
	\override LyricText.self-alignment-X = #LEFT
	Al -- le -- lu -- ia.
}