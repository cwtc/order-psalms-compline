\include "../common_liturgy.ly"

MusicOne = \relative c' {
	\clef bass
	\key e \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	\ph
	g4^\markup { \italic Responsory } g4 g4( a4) a4( g4) a4 b2 b4 g4 a4( b4 a4) g4( e4) g4( a4) a2( g2) \bar "||" 
	\cadenzaOff
}

MusicTwo = \relative c' {
	\clef bass
	\key e \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	\ph
	b1 b4( c4) b4 b4 b4 a2 b4 a4( g4) a4 b2^\markup { \italic {all repeat responsory \Rbar} } \bar "||"
	\cadenzaOff
}

LyricsOne = \lyricmode {
	\override LyricText.self-alignment-X = #LEFT
	\Vbar In -- to thy __ hands __ O Lord, I com -- mend my spi -- rit.
}

LyricsTwo = \lyricmode {
	\override LyricText.self-alignment-X = #LEFT
	\Rbar In -- to thy __ hands __ O Lord, I com -- mend my spi -- rit.
}

LyricsThree = \lyricmode {
	\override LyricText.self-alignment-X = #LEFT
	\Vbar "For thou hast re" -- deem -- ed me, O Lord, O God of truth.
}

\score {
	\new Staff <<
	\new Voice = "One" {
		\MusicOne
	}
	\new Lyrics \lyricsto "One" {
		\LyricsOne
	}
	\new Lyrics \lyricsto "One" {
		\LyricsTwo
	}
	>>
}

\score {
	\new Staff <<
	\new Voice = "Two" {
		\MusicTwo
	}
	\new Lyrics \lyricsto "Two" {
		\LyricsThree
	}
	>>
}
