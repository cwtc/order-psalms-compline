\include "../common_liturgy.ly"

\layout {
  ragged-right = ##f
}

\relative c' {
	\clef bass
	\key e \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	\ph
	g4 g4 g4 e4 e2 \bar "||"
	\ph
	g1 e4 fis2 \bar "||"
	\ph
	g4 g4 fis2( g2) \bar "||"
	\cadenzaOff
}
\addlyrics {
	\override LyricText.self-alignment-X = #LEFT
	\Vbar O Lord, hear our prayer.
	\Rbar "And let our cry come un" -- to thee.
	\Vbar Let us pray.
}