\include "../common_liturgy.ly"

\relative c' {
	\clef bass
	\key e \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	\ph
	g1 fis4 e4 g2 \bar "||" 
	\cadenzaOff
}
\addlyrics {
	\override LyricText.self-alignment-X = #LEFT
	\Vbar "Our help is in the name" of the Lord.
}
\addlyrics {
	\override LyricText.self-alignment-X = #LEFT
	\Rbar "Who hath made hea" -- ven and earth.
}
