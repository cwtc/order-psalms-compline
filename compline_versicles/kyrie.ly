\include "../common_liturgy.ly"

\layout {
  ragged-right = ##f
}

Music = \relative c' {
	\clef bass
	\key e \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	\ph
	g4 g4 g4 e4 fis4 g4 g2 \bar "||" 
	g4 g4 g4 e4 e4 e4 e2 \bar "||" 
	\cadenzaOff
}

MusicTwo = \relative c' {
	\clef bass
	\key e \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	g4 g4 g4 e4 e4 e4 e2 \bar "||" 
	\cadenzaOff
}


Versicle = \lyricmode {
	\override LyricText.self-alignment-X = #LEFT
	\Vbar Lord, have mer -- cy up -- on us.
}

Response = \lyricmode {
	\override LyricText.self-alignment-X = #LEFT
	\Rbar Christ, have mer -- cy up -- on us.
	Lord, have mer -- cy up -- on us.
}

\score {
	\new Staff <<
	\new Voice = "V" {
		\Music
	}
	\new Lyrics \lyricsto "V" {
		\Versicle
	}
	\new Voice = "R" {
		\Music
	}
	\new Lyrics \lyricsto "R" {
		\Response
	}
	>>
}
