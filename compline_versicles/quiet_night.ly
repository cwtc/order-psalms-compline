\include "../common_liturgy.ly"

\layout {
  ragged-right = ##f
}

\relative c' {
	\clef bass
	\key e \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	\ph
	g1 fis4 e4 g2 \bar "|" 
	g4 g4 g4 c,4 c1 \bar "||"
	\ph 
	fis2 g2 \bar "||"
	\cadenzaOff
}
\addlyrics {
	\override LyricText.self-alignment-X = #LEFT
	\Vbar "The Lord Almighty grant us a"
		qui -- et night and a per -- fect end.
	\Rbar A -- men.
}
