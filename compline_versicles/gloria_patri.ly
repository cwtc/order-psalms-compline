\include "../common_liturgy.ly"

\layout {
  ragged-right = ##f
}

Music = \relative c' {
	\clef bass
	\key e \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	\ph
	g1 fis4 g2 \bar "||" 
	\cadenzaOff
}


Versicle = \lyricmode {
	\override LyricText.self-alignment-X = #LEFT
	\Vbar "Glory be to the Father, and to the Son, and to the Ho" -- ly Ghost.
}

Response = \lyricmode {
	\override LyricText.self-alignment-X = #LEFT
	\Rbar "As it was in the beginning,"
}

ResponseTwo = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	\skip 1 % Skip placeholder
	\markup { \hspace #8 "is now, and ever shall be: world without end." } A -- men.
}

\score {
	\new Staff <<
	\new Voice = "V" {
		\Music
	}
	\new Lyrics \lyricsto "V" {
		\Versicle
	}
	\new Voice = "R" {
		\Music
	}
	\new Lyrics \lyricsto "R" {
		\Response
	}
	\new Voice = "R2" {
		\Music
	}
	\new Lyrics \lyricsto "R2" {
		\ResponseTwo
	}
	>>
}