\include "../common_liturgy.ly"

\relative c' {
	\clef bass
	\key e \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	d,4 e4 d4( c4) d4 e4 d4 c4( d4) d2 \bar "|"
	e4( g4) fis4( g4) e4 e4 d4( c4) d4( e4) d2 \bar "||" 
	\cadenzaOff
}
\addlyrics {
	\override LyricText.self-alignment-X = #LEFT
	Have mer -- cy __ up -- on me O __ Lord;
	and __ hear -- ken un -- to __ my __ prayer.
}
