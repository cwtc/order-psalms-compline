\include "../common_liturgy.ly"

\relative c' {
	\clef bass
	\key e \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	\ph
	g1 fis4( g4 fis4 e4 d4 e4 fis4 e2) \bar "||"
	\cadenzaOff
}
\addlyrics {
	\override LyricText.self-alignment-X = #LEFT
	\Vbar "Keep us, O Lord, as the apple of an" eye: __
}
\addlyrics {
	\override LyricText.self-alignment-X = #LEFT
	\Rbar "Hide us under the shadow of thy" wings. __
}
