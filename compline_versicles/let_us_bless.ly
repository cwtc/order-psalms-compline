\include "../common_liturgy.ly"

\layout {
  ragged-last = ##t
}

\relative c' {
	\clef bass
	\key e \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	\ph
	g4 g4 g4( a4) a4 a4( g4 fis2) \bar "||"
	\ph
	g4 g4 a4 a4( g4 fis2) \bar "||"
	\ph
	g1 fis4 e4 g2 \bar "|" \noBreak
	g1 e4 \bar "|"
	g1 c,2 \bar "||"
	\ph
	fis4( g4) g2 \bar "||"
	\cadenzaOff
}
\addlyrics {
	\override LyricText.self-alignment-X = #LEFT
	\Vbar Let us bless the Lord. __
	\Rbar Thanks be to God. __ 
	\Vbar "The Almighty and mer" -- ci -- ful Lord,
		\markup { \concat { "the Father, " \cross "the Son, and the Holy" } } Ghost,
		"bless us and preserve" us.
	\Rbar A -- men.
}