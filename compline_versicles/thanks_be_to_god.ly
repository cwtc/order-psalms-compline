\include "../common_liturgy.ly"

\relative c' {
	\clef bass
	\key e \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	\ph
	g1 c,4 \bar "||" 
	\cadenzaOff
}
\addlyrics {
	\override LyricText.self-alignment-X = #LEFT
	\Vbar "But thou, O Lord, have mercy upon" us.
}
\addlyrics {
	\override LyricText.self-alignment-X = #LEFT
	\Rbar "Thanks be" "to God."
}