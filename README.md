# An order for the Singing of Psalms and the Office of Compline

This repo contains an order for a short service of sung psalms and compline, currently in use at the Church of St. John the Evangelist in Montreal, Quebec. It contains liturgical material from a variety of sources (in good Anglo-Catholic fashion).

The order is typeset in LaTeX, meaning that any changes to its source code must be compiled into a new pdf. The implementation also uses Lilypond for the music engravings, so compilation must be done slightly differently than in a vanilla LaTeX document. To recompile the final pdf `order_for_psalms.pdf`, follow the instructions below.

## Getting up and running

### Prerequisites and background info

First, you will need a TeX distribution that includes XeLaTeX, a typesetting engine that natively handles Unicode input and allows for custom fonts. The world is your oyster here, but the maintainer of this repo likes [TinyTex](https://yihui.name/tinytex/).

Next, you will need to download and install Lilypond, a music engraver that can be integrated with LaTeX documents. Download the appropriate installer for your OS [here](http://lilypond.org/download.html). Note that Lilypond usually stands alone: to make it play nicely with LaTeX, a command-line utility is included called `lilypond-book`. This utility works on the following files in this way:

1. A `.lytex` file is read, which contains normal LaTeX code with inline Lilypond code.

2. Any referenced Lilypond `.ly` files in the `.lytex` file are individually rendered into music snippets, each in `.pdf` form.

3. A new `.tex` file is then created from the original `.lytex` file and the created snippets, where the original references to `.ly` files are replaced by the newly rendered `.pdf`s.

### Compilation

After making your desired changes to any of the code, you then need to compile the document. Two scripts, `compile.bat` and `compile.sh` for Windows and Mac/Linux respectively, are provided for this purpose. This script basically does the following:

1. Runs `lilypond-book` on the original `.lytex` file.
2. Runs the resulting `.tex` file through XeTeX, creating a final `.pdf` output file.
3. Cleans up all auxiliary created files.

Run the script like this, from the root of the repo:

```
compile.bat
```

or

```
./compile.sh
```

After a long series of output logs, the final `.pdf` should appear in the root of the repo.
