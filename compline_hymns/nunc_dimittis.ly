\include "../common_liturgy.ly"

\layout {
  ragged-last = ##t
}

\relative c' {
	\clef bass
	\key bes \major
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	f,4^\markup { \italic Antiphon } g4( bes4) bes4( c4) bes4 bes4( a4) g4( a4) g4 f2 \bar "|"
	f4 a4( bes4) c4 bes4( g4) bes4 a2 \bar "|"
	a4( f4) a4 c4 bes4 g4 bes4 bes4 a4( bes2) \bar "|"
	g4 g4 g4 f4( ees4) f4( g4) f4( ees4) d2 \bar "||"

	f4^\markup { \italic Canticle } g4 bes4 bes4 bes4 c4 bes4 bes4 a4 bes2 \bar "|" \noBreak
	bes2 g4 bes4 bes4 a2( g2) \bar "|"
	bes4 bes4 c4 bes4 bes2 bes4 g4 bes4 a2( g2) \bar "|"
	bes4 c4 bes4 bes4 a2 bes2 bes4 bes4 bes4 bes4 g4 bes4 a2( g2) \bar "|"
	f4 g4 bes4 bes4 bes4 c4 bes4 bes4 a2 bes2 \bar "|" \noBreak
	bes1 g4 bes4 bes4 a2( g2) \bar "||"

	f4 g4 bes4 bes4 bes4 c4 bes4 bes4 bes4 a4 bes2 \bar "|"
	bes4 bes4 g4 bes4 bes4 a2( g2) \bar "|"
	f4 g4 bes2 a4 bes2 \bar "|"
	bes4 bes4 bes4 c4 bes4 a4 bes2 \bar "|"
	bes4 bes4 g4 bes4 bes4 a2( g2)^\markup { \italic { repeat antiphon } } \bar "||"
	\cadenzaOff
}
\addlyrics {
	\override LyricText.self-alignment-X = #LEFT
	Pre -- serve us, __ O Lord, while wa -- king,
	and guard us while sleep -- ing,
	that a -- wake we may be with Christ
	and in peace may take our rest.

	\markup { \concat { \cross Lord, } } now lettest thou thy ser -- vant depart in peace,
	accord -- ing to thy word. __
	For mine eyes have seen thy sal -- va -- tion, __
	which thou hast pre -- par -- ed before the face of all peo -- ple: __
	to be a light to light -- en the Gen -- tiles,
	"and to be the glory of thy peo" -- ple Is -- ra -- el. __

	Glo -- ry be to the Fa -- ther, and to the Son,
	and to the Ho -- ly Ghost.
	As it "was in the be" -- gin -- ning,
	is now, and ev -- er shall be:
	world with -- out end. A -- men. __
}