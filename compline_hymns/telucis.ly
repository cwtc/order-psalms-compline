\include "../common_liturgy.ly"

\layout {
  ragged-last = ##t
}

MusicOne = \relative c' {
	\clef bass
	\key bes \major
	\override Staff.TimeSignature #'stencil = ##f
	\override Score.BarNumber #'transparent = ##t 
	\hide Staff.Stem
	\cadenzaOn
	g4 bes4 bes4 bes4 bes4 c4 bes4 a2 \bar "|"
	g4 g4 g4 g4 a4 a4 g4 f2 \bar "|" \break
	a4 a4 g4 f4 d4 f4 g4 g2 \bar "|" \noBreak
	bes4 bes4 bes4 bes4 g4 a4 g4 f2 \bar "||" \break
	f4( g4 f4) f2 \bar "||" 
	\cadenzaOff
}

VerseOne = \lyricmode {
	%\override LyricText.self-alignment-X = #LEFT
	Be -- fore the end -- ing of the day,
	Cre -- a -- tor of the world, we pray,
	that with thy wont -- ed fa -- vour thou
	wouldst be our guard and keep -- er now.
}

VerseTwo = \lyricmode {
	%\override LyricText.self-alignment-X = #LEFT
	From all ill dreams de -- fend our eyes,
	from night -- ly fears and fan -- ta -- sies;
	tread un -- der foot our ghost -- ly foe,
	that no pol -- lu -- tion we may know.
	A __ __ men.
}

VerseThree = \lyricmode {
	%\override LyricText.self-alignment-X = #LEFT
	O Fa -- ther, that we ask be done,
	through Je -- sus Christ thine on -- ly Son,
	who with the Ho -- ly Ghost and thee
	doth live and reign e -- ter -- nal -- ly.
}

\score {
	\new Staff <<
	\new Voice = "V1" {
		\MusicOne
	}
	\new Voice = "V2" {
		\MusicOne
	}
	\new Voice = "V3" {
		\MusicOne
	}
	\new Lyrics \lyricsto "V1" {
		\VerseOne
	}
	\new Lyrics \lyricsto "V2" {
		\VerseTwo
	}
	\new Lyrics \lyricsto "V3" {
		\VerseThree
	}
	>>
}
