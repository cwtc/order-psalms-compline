\include "../common_liturgy.ly"

\relative c' {
	\clef bass
	\key c \major
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	g4 a4 c1 d4 \parenthesize c2 \bar "|" 
	c1 b4 c4 a4 g2 \bar "||"
	\cadenzaOff
}
