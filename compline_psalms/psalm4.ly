\include "../common_liturgy.ly"

\relative c' {
	\clef bass
	\key c \major
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	c,4 d4 f1 g4 \parenthesize f2 \bar "|" 
	f1 e4 c4 d2 \bar "||"
	\cadenzaOff
}
