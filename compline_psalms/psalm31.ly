\include "../common_liturgy.ly"

\relative c' {
	\clef bass
	\key c \major
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	g4 a4( c1) d4 c4 b4 c2 \bar "|" 
	c1 a4 c4 b4( a2) \bar "||"
	\cadenzaOff
}
