\include "../common_liturgy.ly"

\relative c' {
	\clef bass
	\key c \major
	\override Staff.TimeSignature #'stencil = ##f 
	\hide Staff.Stem
	\cadenzaOn
	f,4 g4 a1 g4 a2 \bar "|" 
	a1 g4 f4 g4( a4) \parenthesize g4 g2 \bar "||"
	\cadenzaOff
}
