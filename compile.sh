lilypond-book --output=out --pdf order_for_psalms.lytex 
cd out/
xelatex order_for_psalms
mv order_for_psalms.pdf ../order_for_psalms.pdf
cd ..
rm -rf out/
